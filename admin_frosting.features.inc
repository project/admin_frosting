<?php
/**
 * @file
 * admin_frosting.features.inc
 */

/**
 * Implements hook_views_api().
 */
function admin_frosting_views_api() {
  return array("version" => "3.0");
}
