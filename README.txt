-- SUMMARY --

Admin Frosting is a feature module that replaces Drupal's default Content, 
Comment and People (User) administration screens utilizing Views, 
Views Bulk Operations (VBO) and Rules. Out of the box, it comes with a ton 
more options than Drupal core gives you.

For a full description of the module, visit the project page:

http://drupal.org/project/admin_frosting

To submit bug reports and feature suggestions, or to track changes:

http://drupal.org/node/add/project-issue/admin_frosting


-- REQUIREMENTS --

Modules:

Views
http://www.drupal.org/project/views

Views Bulk Operations (VBO)
http://www.drupal.org/project/views_bulk_operations

Rules
http://www.drupal.org/project/rules

Better Exposed Fitlers
http://www.drupal.org/project/better_exposed_filters

Features
http://www.drupal.org/project/features

-- INSTALLATION --

1) Download all required modules and place them in your modules folder
    (usually /sites/all/modules).

2) Enable module dependencies and Admin Frosting at admin/modules.

3) Optional: Enable Views UI. This will be required to customize your views.


--USAGE--

Content (/admin/content), Comment (/admin/content/comment and 
/admin/content/comment/approval) and User/People (/admin/people) 
Administration pages are now replaced with one's from Admin Frosting.

-- CONFIGURATION --

None

-- CUSTOMIZATION --

@TODO

Please note that any of these customizations will override the default
settings provided by this module and will store your customizations
in the database. To undo your changes, click 'revert' on the view and
it will return to it's original state.


-- TROUBLESHOOTING --

none.


-- FAQ --

@TODO

-- CONTACT --

Current maintainers:
*Nico Zdunich (nicoz) - http://drupal.org/user/693674

This project has been sponsored by:
*Pixel Sweatshop - http://www.pixelsweatshop.com
